# Use a lightweight alpine image for the runtime container
FROM alpine:latest

# Copy the compiled binary from the local machine
COPY ./main /app/main

# Expose port 8080 for the app to listen on
EXPOSE 8080

# Run the binary
CMD ["/app/main"]
