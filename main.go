package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

func getDrivers(db *sql.DB, w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	rows, err := db.Query("SELECT * FROM Drivers")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	drivers := []map[string]string{}
	for rows.Next() {
		var id, name, imageURL string
		rows.Scan(&id, &name, &imageURL)
		drivers = append(drivers, map[string]string{
			"ID":       id,
			"Name":     name,
			"ImageURL": imageURL,
		})
	}
	json.NewEncoder(w).Encode(drivers)
}

func getLocation(db *sql.DB, w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	vars := mux.Vars(r)
	driverID := vars["driverID"]

	var lastLat, lastLng float64

	// Get the last location of the driver from the database
	err := db.QueryRow("SELECT Latitude, Longitude FROM Locations WHERE DriverID = ? ORDER BY ID DESC LIMIT 1", driverID).Scan(&lastLat, &lastLng)
	if err != nil && err != sql.ErrNoRows {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Approximate increment of 5 meters in latitude and longitude
	if lastLat == 0 {
		lastLat = 12.971598

	}
	if lastLng == 0 {
		lastLng = 77.594566
	}
	latIncrement := 0.000045 // approx 5m in latitude
	lngIncrement := 0.000045 // approx 5m in longitude

	newLat := lastLat + latIncrement
	newLng := lastLng + lngIncrement

	// Saving new location into DB
	_, err = db.Exec("INSERT INTO Locations (DriverID, Latitude, Longitude) VALUES (?, ?, ?)", driverID, newLat, newLng)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(map[string]float64{"Latitude": newLat, "Longitude": newLng})
}

func main() {
	r := mux.NewRouter()

	db, err := sql.Open("mysql", "root:password@password@123#@tcp(35.200.225.252:3306)/DriverDB")
	if err != nil {
		panic(err.Error())
	}

	r.HandleFunc("/drivers", func(w http.ResponseWriter, r *http.Request) {
		getDrivers(db, w, r)
	}).Methods("GET")

	r.HandleFunc("/location/{driverID}", func(w http.ResponseWriter, r *http.Request) {
		getLocation(db, w, r)
	}).Methods("GET")

	http.Handle("/", r)
	fmt.Println("Server running on port 8081")
	http.ListenAndServe(":8081", nil)
}
